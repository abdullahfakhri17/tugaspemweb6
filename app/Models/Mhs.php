<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mhs extends Model
{
    use HasFactory;
    protected $table = 'mhs_225150409111021';
    protected $fillable = ['nim', 'nama'];
}
